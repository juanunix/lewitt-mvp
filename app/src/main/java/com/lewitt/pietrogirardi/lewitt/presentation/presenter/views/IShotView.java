package com.lewitt.pietrogirardi.lewitt.presentation.presenter.views;

import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;

import java.util.List;

/**
 * Created by pietrogirardi on 09/07/16.
 */
public interface IShotView extends View{
    void showLoading();
    void hideLoading();
    void showItems(List<Shot> images);
    void showError(String error);
    void showEpisodeDetail(Shot shot);
}
