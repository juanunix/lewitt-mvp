package com.lewitt.pietrogirardi.lewitt.domain.interactor;

public interface IUiThreadExecutor {
    void execute(Runnable runnable);
}
