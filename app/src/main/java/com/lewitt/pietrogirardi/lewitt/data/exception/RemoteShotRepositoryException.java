package com.lewitt.pietrogirardi.lewitt.data.exception;

/**
 * Created by p.girardi on 7/11/2016.
 */

public class RemoteShotRepositoryException extends Exception{

    public RemoteShotRepositoryException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public RemoteShotRepositoryException(String detailMessage) {
        super(detailMessage);
    }

}
