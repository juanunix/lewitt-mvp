package com.lewitt.pietrogirardi.lewitt.data.di;

import com.lewitt.pietrogirardi.lewitt.data.api.IShotAPI;
import com.lewitt.pietrogirardi.lewitt.data.remote.RemoteShotRepository;
import com.lewitt.pietrogirardi.lewitt.domain.entities.Shot;
import com.lewitt.pietrogirardi.lewitt.domain.repository.ShotRepository;

import java.io.IOException;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by p.girardi on 7/8/2016.
 */

@Module
public class ShotDataModel {
    public static final String ENDPOINT = "http://api.dribbble.com/v1/";

    @Provides
    public IShotAPI provideShotApi() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(IShotAPI.class);
    }

    @Provides public ShotRepository provideShotRepository(RemoteShotRepository remoteShotRepository) {
        return remoteShotRepository;
    }
}