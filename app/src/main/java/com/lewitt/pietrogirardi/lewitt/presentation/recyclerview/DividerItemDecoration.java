package com.lewitt.pietrogirardi.lewitt.presentation.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

/**
 * Created by pietrogirardi on 10/07/16.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration{

    private Context mContext;
    private int mOrientation;
    private int mColumnNum;

    public DividerItemDecoration(Context mContext, int mOrientation) {
        this.mContext = mContext;
        this.mOrientation = mOrientation;
    }

    public DividerItemDecoration(Context mContext, int mOrientation, int mColumnNum) {
        this(mContext, mOrientation);
        this.mColumnNum = mColumnNum;
    }
}
